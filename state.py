from sqlalchemy import create_engine,Table, Column,Integer, String, MetaData, Boolean

engine = create_engine('sqlite:///statut.db', echo = True)
meta = MetaData()

states = Table(
    'states', meta,
    Column('id', Integer, primary_key = True),
    Column('public', Boolean),
    Column('type', String)
)
meta.create_all(engine)